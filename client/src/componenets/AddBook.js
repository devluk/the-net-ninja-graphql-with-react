import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries/queries';

class AddBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      genre: '',
      authorId: '',
      formErrors: false,
      formSuccess: false
    }
  }
  displayAuthors() {
    var data = this.props.getAuthorsQuery;
    if(data.loading) {
      return( <option disabled>Loading Authors...</option> );
    } else {
      return data.authors.map(author => {
        return( <option key={ author.id } value={ author.id } >{ author.name }</option>);
      });
    }
  }
  submitForm(e) {
    e.preventDefault();

    const { name, genre, authorId } = this.state;

    if(name && genre && authorId) {
      this.props.addBookMutation({
        variables: {
          name: this.state.name,
          genre: this.state.genre,
          authorId: this.state.authorId
        },
        refetchQueries: [{ query: getBooksQuery }]
      });
      this.setState({ formErrors: false });
      this.setState({ formSuccess: true });
    } else {
      this.setState({ formErrors: true });
      this.setState({ formSuccess: false });
    }
  }
  render() {
    return (
      <form id="add-book" onSubmit={ this.submitForm.bind(this) }>

        <div className="field">
          <label>Book name:</label>
          <input type="text" onChange={ (e) => this.setState({ name: e.target.value })} />
        </div>

        <div className="field">
          <label>Genre:</label>
          <input type="text" onChange={ (e) => this.setState({ genre: e.target.value })} />
        </div>

        <div className="field">
          <label>Author:</label>
          <select onChange={ (e) => this.setState({ authorId: e.target.value })} >
            <option value="">Select author</option>
            { this.displayAuthors() }
          </select>
        </div>

        <button>+</button>

        <div className="messages">
          <span className={"error " + (this.state.formErrors ? 'show' : '')}>Please fill in all fields</span>
          <span className={"success " + (this.state.formSuccess ? 'show' : '')}>Book successfully added</span>
        </div>

      </form>
    );
  }
}

export default compose(
  graphql(getAuthorsQuery, { name: "getAuthorsQuery" }),
  graphql(addBookMutation, { name: "addBookMutation" })
)(AddBook);
