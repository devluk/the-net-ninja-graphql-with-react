import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { getAuthorsQuery, addAuthorMutation } from '../queries/queries';

class AddAuthor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: '',
      formErrors: false,
      formSuccess: false
    }
  }
  submitForm(e) {
    e.preventDefault();

    const { name, age } = this.state;

    if(name && age) {
      this.props.addAuthorMutation({
        variables: {
          name: this.state.name,
          age: this.state.age
        },
        refetchQueries: [{ query: getAuthorsQuery }]
      });
      this.setState({ formErrors: false });
      this.setState({ formSuccess: true });
    } else {
      this.setState({ formErrors: true });
      this.setState({ formSuccess: false });
    }
  }
  render() {
    return (
      <form id="add-author" onSubmit={ this.submitForm.bind(this) }>

        <div className="field">
          <label>Author name:</label>
          <input type="text" onChange={ (e) => this.setState({ name: e.target.value })} />
        </div>

        <div className="field">
          <label>Age:</label>
          <input type="number" onChange={ (e) => this.setState({ age: e.target.value })} />
        </div>

        <button>+</button>

        <div className="messages">
          <span className={"error " + (this.state.formErrors ? 'show' : '')}>Please fill in all fields</span>
          <span className={"success " + (this.state.formSuccess ? 'show' : '')}>Author successfully added</span>
        </div>

      </form>
    );
  }
}

export default compose(
  graphql(getAuthorsQuery, { name: "getAuthorsQuery" }),
  graphql(addAuthorMutation, { name: "addAuthorMutation" })
)(AddAuthor);
