import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider} from 'react-apollo';

// components
import BookList from './componenets/BookList';
import AddBook from './componenets/AddBook';
import AddAuthor from './componenets/AddAuthor';

// apollo client setup
const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div id="main">
          <h1>Ninja's Reading List</h1>
          <BookList />
          <AddBook />
          <AddAuthor />
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
