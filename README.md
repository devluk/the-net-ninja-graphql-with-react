Based on https://www.youtube.com/playlist?list=PL4cUxeGkcC9iK6Qhn-QLcXCXPQUov1U7f with some improvements:  
- Add AddAuthor component
- Pass newly added Author to the AddBook dropdown
- Add form validation messages

# Server setup
- `cd server`  
- We'll be using an online mongo db - mLab  
  - Head over to https://mlab.com  
  - Create databse and user  
  - Change `env.default.js` to `env.js`  
  - Update `env.js` with access uri (with `<dbuser>` and `<dbpassword>`)  
- `npm install`  
- `npm start` ( in case you are missing nodemon, install by running `npm install nodemon -g` ). 


# Client setup
- `cd client`  
- `npm install`  
- `npm start`  


# TODO
[ ] Improve css (handle mobile)
[ ] Improve UX (clear form for newly added Author/Book)
[ ] Improve validation (forbid douyble Authors/Books)
